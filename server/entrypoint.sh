#!/bin/bash

gunicorn --bind 0.0.0.0:5000 -w 1 --threads 100 server:app
