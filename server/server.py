from flask import Flask, request
from flask_socketio import SocketIO, send
from utils import EventNames

app = Flask(__name__)

app.config['SECRET_KEY'] = 'secret!'

socketio = SocketIO(app, cors_allowed_origins="*", logger=True)


@socketio.on('message')
def message(message):
  print(message)
  socketio.emit(EventNames['SWITCH_BUTTON'], {'switchOn': not message['switchOn']})


@socketio.on_error_default
def default_error_handler(e):
  print('ERROR!!!')
  print(e)
  print(request.event["message"]) # "my error event"
  print(request.event["args"])    # (data,)


@app.route('/')
def index():
  socketio.emit(EventNames['SWITCH_BUTTON'], {'switchOn': True})
  return 'Hello, world!'


if __name__ == '__main__':
  socketio.run(app)
