# Пример back/front (python Flask/react) приложения с WebSocket
## Зависимости
Для работы нужен docker и node.js (лучше испльзовать nvm)
[nvm for windows](https://github.com/coreybutler/nvm-windows)
[nvm linux](https://github.com/nvm-sh/nvm)

## Запуск
В корневой директории:\
`./bin/start.sh`

Запускает 2 приложения на фронте [http://localhost:4000](http://localhost:4000),
и flask-server [http://localhost:5000](http://localhost:5000)

### Запуск контейнера сервера
В директории server:\
$>`docker build -t controllers:server .`\
$>`docker run -p 5000:5000 controllers:server`

### Запуск контейнера фронта
В директории front:\
$>`docker build -t controllers:front .`\
$>`docker run -it --rm -v ${PWD}:/app -v /app/node_modules -p 4000:3000 -e CHOKIDAR_USEPOLLING=true controllers:front`

# VIDEO
![](running_app.mp4)
