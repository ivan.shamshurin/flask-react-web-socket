import { useContext, useState } from 'react';
import { useDrag } from 'react-dnd';

import { SocketContext } from '../App';
import { useSubscription } from './hooks';
import { StyledCircle } from './styled';
import { EventNames, ItemTypes } from './types';

export const Circle = () => {
  const [isOn, setIsOn] = useState(false);
  const socket = useContext(SocketContext);
  const [{ opacity }, dragRef] = useDrag(
    () => ({
      type: ItemTypes.CIRCLE,
      item: { text: isOn ? 'On' : 'Off' },
      collect: (monitor) => ({
        opacity: monitor.isDragging() ? 0.5 : 1,
      }),
    }),
    [],
  );
  useSubscription({
    socket: socket,
    eventName: EventNames.SWITCH_BUTTON,
    callbackFn: (data: { switchOn: boolean }) => {
      console.log('>>>>>>>>>>>>>>>> WS EVENT: ', data);
      setIsOn(data.switchOn);
    },
  });

  return <StyledCircle ref={dragRef} isOn={isOn} opacity={opacity} />;
};
