import React, { useContext, useState } from 'react';

import { SocketContext } from '../App';
import { useSubscription } from './hooks';
import { StyledButton } from './styled';
import { EventNames } from './types';

export const SwitchButton = () => {
  const [isOn, setIsOn] = useState(false);
  const socket = useContext(SocketContext);

  useSubscription({
    socket: socket,
    eventName: EventNames.SWITCH_BUTTON,
    callbackFn: (data: { switchOn: boolean }) => {
      console.log('>>>>>>>>>>>>>>>> WS EVENT: ', data);
      setIsOn(data.switchOn);
    },
  });

  const toggleButton = () => {
    socket?.emit('message', { switchOn: isOn });
  };

  return (
    <StyledButton variant='contained' color='primary' onClick={toggleButton}>
      {isOn ? 'Включено' : 'Выключено'}
    </StyledButton>
  );
};
