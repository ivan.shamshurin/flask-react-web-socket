import { useEffect } from 'react';
import { Socket } from 'socket.io-client';

export function useSubscription({
  socket,
  eventName,
  callbackFn,
}: {
  socket: Socket | null;
  eventName: string;
  callbackFn: (event: any) => void;
}): void {
  useEffect(() => {
    const subscription = socket?.on(eventName, callbackFn);
    return () => {
      subscription?.off();
    };
  }, [socket]);
}
