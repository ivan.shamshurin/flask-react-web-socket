import styled from '@emotion/styled';
import { Button } from '@material-ui/core';

export const StyledButton = styled(Button)`
  width: 150px;
`;

export const StyledCircle = styled.div<{ isOn: boolean; opacity: number }>`
  background-color: ${(props) => (props.isOn ? 'red' : 'blue')};
  opacity: ${(props) => props.opacity};
  width: 100px;
  height: 100px;
  border-radius: 50px;
`;
