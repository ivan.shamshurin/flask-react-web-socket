import React, { createContext, useEffect, useState } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { Socket, io } from 'socket.io-client';

import './App.scss';
import { Circle } from './components/Circle';
import { SwitchButton } from './components/SwitchButton';

export const SocketContext = createContext<Socket | null>(null);

const useWebSocket = (): Socket | null => {
  const [socket, setSocket] = useState<Socket | null>(null);
  useEffect(() => {
    const socketInstance = io('http://localhost:5000');
    setSocket(socketInstance);
    return () => {
      socketInstance.disconnect();
    };
  }, []);
  return socket;
};

const App = () => {
  const socket = useWebSocket();
  return (
    <SocketContext.Provider value={socket}>
      <DndProvider backend={HTML5Backend}>
        <div className='App'>
          <div>
            <SwitchButton />
            <Circle />
          </div>
        </div>
      </DndProvider>
    </SocketContext.Provider>
  );
};

export default App;
